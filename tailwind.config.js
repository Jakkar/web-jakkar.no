module.exports = {
    content: ["./public/**/*.{html,js}"],
    theme: {
        extend: {
            fontFamily: {
                'sans': ['Fira Sans'],
            },
        },
        container: {
            center: true,
        },
    },
    plugins: [],
}
